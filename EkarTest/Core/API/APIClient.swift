//
//  APIClient.swift
//  EkarTest
//
//  Created by karthik damodara on 3/8/20.
//  Copyright © 2020 karthik damodara. All rights reserved.
//

import Foundation

enum Result<T> {
    case success(_ data: T)
    case failure(Error?)
    
    var value: T? {
        guard case .success(let value) = self else { return nil }
        return value
    }
    
    var error: Error? {
        guard case .failure(let error) = self else { return nil }
        return error
    }
}

class APIClient {
    
    private lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        decoder.dateDecodingStrategy = .iso8601
        return decoder
    }()
    
    private let baseURL: String

    init(baseURL: String) {
        self.baseURL = baseURL
    }

    
    func perform<T: Codable>(_ path: String, onCompletion: @escaping ((Result<T>) -> Void)) {
        let fullPath = baseURL + path
        let url = URL(string: fullPath)!
        let session = URLSession.shared
        let task = session.dataTask(with: url) { data, response, error in
            guard let data = data else {
                onCompletion(.failure(error))
                return
            }
            
            do {
                let decoder = self.decoder
                let decodedData = try decoder.decode(T.self, from: data)
                onCompletion(.success(decodedData))
            } catch {
                onCompletion(.failure(error))
            }
        }
        
        task.resume()
    }
    
}
