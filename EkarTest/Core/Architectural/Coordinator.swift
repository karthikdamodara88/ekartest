//
//  Coordinator.swift
//  EkarTest
//
//  Created by karthik damodara on 3/8/20.
//  Copyright © 2020 karthik damodara. All rights reserved.
//

import UIKit

class Coordinator {
    
    weak var parentCoordinator: Coordinator?
    
    private weak var _root: UIViewController?
    
    var isRootViewControllerLoaded: Bool {
        return _root != nil
    }
    
    var rootViewController: UIViewController {
        if let root = _root { return root }
        
        let main = createRootViewController()
        _root = main
        return main
    }
    
    var topParentCoordinator: Coordinator? {
        return parentCoordinator?.topParentCoordinator ?? parentCoordinator
    }
    
    func replaceRootViewController(with viewController: UIViewController) {
        _root = viewController
    }
    
    func createRootViewController() -> UIViewController {
        fatalError("Define a controller to start")
    }
}
