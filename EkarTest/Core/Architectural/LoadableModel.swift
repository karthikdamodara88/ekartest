//
//  LoadableModel.swift
//  EkarTest
//
//  Created by karthik damodara on 3/8/20.
//  Copyright © 2020 karthik damodara. All rights reserved.
//

import Foundation

enum LoadableModel<T> {
    
    case loading(T?)
    case none
    case error(Error)
    case some(T)
    
    var isLoading: Bool {
        switch self {
        case .loading: return true
        case .none, .error, .some: return false
        }
    }
    
    var isNil: Bool {
        switch self {
        case .none: return true
        case .loading, .error, .some: return false
        }
    }
    
    var value: T? {
        switch self {
        case let .some(value): return value
        case let .loading(value): return value
        case .none, .error: return nil
        }
    }
    
    var error: Error? {
        switch self {
        case let .error(error): return error
        case .none, .loading, .some: return nil
        }
    }
}
