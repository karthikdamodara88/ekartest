//
//  BuildConfiguration.swift
//  EkarTest
//
//  Created by karthik damodara on 3/8/20.
//  Copyright © 2020 karthik damodara. All rights reserved.
//

import Foundation

class BuildConfiguration {
    static let sharedInstance = BuildConfiguration()

    var baseURL: String {
        return getBundleValue(for: "CN_BASE_URL")
    }

    private func getBundleValue(for key: String) -> String {
        return Bundle.main.infoDictionary?[key] as! String
    }
}
