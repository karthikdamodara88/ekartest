//
//  NavigationController.swift
//  EkarTest
//
//  Created by karthik damodara on 3/8/20.
//  Copyright © 2020 karthik damodara. All rights reserved.
//

import UIKit

final class NavigationController: UINavigationController {
    
    convenience init() {
        self.init(rootViewController: UIViewController())
    }
    
    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
