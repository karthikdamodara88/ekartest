//
//  Cells+Dequeue.swift
//  EkarTest
//
//  Created by karthik damodara on 3/8/20.
//  Copyright © 2020 karthik damodara. All rights reserved.
//

import UIKit

protocol ReusableView {}
extension ReusableView {
    static var reuseId: String {
        return "\(self)"
    }
    
    static var nib: UINib {
        return UINib(nibName: reuseId, bundle: Bundle.main)
    }
}

extension UITableViewCell: ReusableView {}
extension UICollectionReusableView: ReusableView {}
extension UITableViewHeaderFooterView: ReusableView {}
