//
//  AppCoordinator.swift
//  EkarTest
//
//  Created by karthik damodara on 3/8/20.
//  Copyright © 2020 karthik damodara. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator {
    private(set) lazy var window: UIWindow = {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.makeKeyAndVisible()
        return window
    }()
    
    
    override func createRootViewController() -> UIViewController {
        return MapCoordinator().rootViewController
    }

}
