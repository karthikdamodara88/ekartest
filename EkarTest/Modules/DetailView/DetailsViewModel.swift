//
//  DetailsViewModel.swift
//  EkarTest
//
//  Created by karthik damodara on 3/8/20.
//  Copyright © 2020 karthik damodara. All rights reserved.
//

import Foundation

struct DetailsViewModel {
    var numberOfItems: Int {
        return imagePath.count
    }
    
    private let imagePath: [String]
    
    init(imagePath: [String]) {
        self.imagePath = imagePath
    }
    
    func cellViewModel(at indexPath: IndexPath) -> Any? {
        guard indexPath.row < imagePath.count else { return nil }
        return DetailCollectionCellViewModel(imagePath: imagePath[indexPath.row])
    }
}
