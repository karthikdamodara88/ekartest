//
//  DetailCoordinator.swift
//  EkarTest
//
//  Created by karthik damodara on 3/8/20.
//  Copyright © 2020 karthik damodara. All rights reserved.
//

import UIKit

final class DetailCoordinator: AppCoordinator {
    
    override func createRootViewController() -> UIViewController {
        let viewModel = DetailsViewModel(imagePath: [])
        return DetailViewController(with: viewModel)
    }
}
