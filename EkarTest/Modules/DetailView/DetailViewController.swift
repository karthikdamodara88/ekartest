//
//  DetailViewController.swift
//  EkarTest
//
//  Created by karthik damodara on 3/8/20.
//  Copyright © 2020 karthik damodara. All rights reserved.
//

import UIKit


final class DetailViewController: UIViewController {
    
    private enum Constants {
        static let margin = 16
    }
    
    private let collectionView = UICollectionView()
    private let actionButton = UIButton()
    private let textField = UITextField()
    private var viewModel: DetailsViewModel
    
    init(with viewModel: DetailsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupLayout()
        setupStyle()
    }
    
    private func setupView() {
        view.addSubview(collectionView)
        view.addSubview(actionButton)
        view.addSubview(textField)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.register(DetailCollectionViewCell.self, forCellWithReuseIdentifier: DetailCollectionViewCell.reuseId)
    }
    
    private func setupLayout() {
        actionButton.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview().inset(Constants.margin)
        }
        
        textField.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview().inset(Constants.margin)
            make.bottom.equalTo(actionButton.snp.top).offset(Constants.margin)
        }
        
        collectionView.snp.makeConstraints { make in
            make.leading.trailing.top.equalToSuperview().inset(Constants.margin)
            make.bottom.equalTo(textField.snp.top).offset(Constants.margin)
        }
    }
    
    private func setupStyle() {
        view.backgroundColor = .white
    }
    
    func configure(with viewModel: DetailsViewModel) {
        self.viewModel = viewModel
        collectionView.reloadData()
    }
}

extension DetailViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch viewModel.cellViewModel(at: indexPath) {
        case let cellViewModel as DetailCollectionCellViewModel:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DetailCollectionViewCell.reuseId,
                                                          for: indexPath)
            (cell as? DetailCollectionViewCell)?.configure(with: cellViewModel)
            return cell
        default:
            return UICollectionViewCell()
        }
    }
}

extension DetailViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}
