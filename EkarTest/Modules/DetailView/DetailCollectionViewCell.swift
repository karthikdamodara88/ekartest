//
//  DetailCollectionViewCell.swift
//  EkarTest
//
//  Created by karthik damodara on 3/8/20.
//  Copyright © 2020 karthik damodara. All rights reserved.
//

import UIKit

struct DetailCollectionCellViewModel {
    let imagePath: String
}

final class DetailCollectionViewCell: UICollectionViewCell {
    private enum Constants {
        static let margin = 16
    }
    
    private let imageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
        setupLayout()
        setupStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func setupView() {
        contentView.addSubview(imageView)
    }
    
    private func setupLayout() {
        imageView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(Constants.margin)
        }
    }
    
    private func setupStyle() {
        contentView.backgroundColor = .white
    }
    
    func configure(with viewModel: DetailCollectionCellViewModel) {
        imageView.image = UIImage(named: viewModel.imagePath)
    }
}
