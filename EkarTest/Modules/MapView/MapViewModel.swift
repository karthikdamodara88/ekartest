//
//  MapViewModel.swift
//  EkarTest
//
//  Created by karthik damodara on 3/8/20.
//  Copyright © 2020 karthik damodara. All rights reserved.
//

import Foundation

struct MapViewModel {

    private let interactor: MapInteractor
    private let coordinator: MapCoordinator
    
    init(with interactor: MapInteractor, coordinator: MapCoordinator) {
        self.interactor = interactor
        self.coordinator = coordinator
    }
    
    func fetchData() {
        interactor.fetchData()
    }
    
    var annotation: MapAnnotation? {
        return interactor.annotation
    }
}
