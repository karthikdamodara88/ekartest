//
//  MapRepository.swift
//  EkarTest
//
//  Created by karthik damodara on 3/8/20.
//  Copyright © 2020 karthik damodara. All rights reserved.
//

import Foundation

final class MapRespository {
    
    private let apiClient: APIClient
    
    init(apiClient: APIClient) {
        self.apiClient = apiClient
    }

    func getMapMarkers(onCompletion: @escaping (Result<MapModel>) -> Void) {
        apiClient.perform("bins/a9bw2", onCompletion: onCompletion)
    }
}
