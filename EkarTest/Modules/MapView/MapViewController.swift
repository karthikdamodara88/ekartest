//
//  MapViewController.swift
//  EkarTest
//
//  Created by karthik damodara on 3/8/20.
//  Copyright © 2020 karthik damodara. All rights reserved.
//

import UIKit
import MapKit
import SnapKit
import CoreLocation

final class MapViewController: UIViewController, UpdateableView {
    
    private enum Constants {
        static let height = 16
        static let regionRadius: CLLocationDistance = 1000
        static let initialLocation = CLLocation(latitude: 25.2048, longitude: 55.2708)
    }
    
    private let mapView = MKMapView()
    private var viewModel: MapViewModel
    
    init(with viewModel: MapViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupLayout()
        setupStyle()
        viewModel.fetchData()
    }
    
    private func setupView() {
        view.addSubview(mapView)
        let coordinateRegion = MKCoordinateRegion(center: Constants.initialLocation.coordinate,
                                                  latitudinalMeters: Constants.regionRadius,
                                                  longitudinalMeters: Constants.regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    private func setupLayout() {
        mapView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    private func setupStyle() {
        view.backgroundColor = .white
        let image = UIImage(named: "ekar-logo.png")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        navigationItem.titleView = imageView
        
    }
    
    func configure(with viewModel: MapViewModel) {
        self.viewModel = viewModel
        viewModel.annotation.map(mapView.addAnnotation)
    }
    
}
