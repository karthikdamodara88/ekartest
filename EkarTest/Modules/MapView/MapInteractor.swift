//
//  MapInteractor.swift
//  EkarTest
//
//  Created by karthik damodara on 3/8/20.
//  Copyright © 2020 karthik damodara. All rights reserved.
//

import Foundation
import MapKit

struct MapModel: Codable {
    let id: Int
    let title: String
    let location: String
    let latitude: Double
    let longitude: Double
}

final class MapInteractor: Interactor {
    var modelDidUpdate: (() -> Void)?
    var model: LoadableModel<MapModel> {
        didSet {
            modelDidUpdate?()
        }
    }
    
    private let mapRepository: MapRespository
    
    var annotation: MapAnnotation? {
        guard let value = model.value else { return nil }
        let location = CLLocation(latitude: value.latitude, longitude: value.longitude)
        return MapAnnotation(title: value.title, locationName: value.location, coordinate: location.coordinate)
    }
    
    init(mapRepository: MapRespository) {
        self.mapRepository = mapRepository
        self.model = .none
    }
    
    func fetchData() {
        mapRepository.getMapMarkers { result in
            switch result {
            case.success(let value):
                self.model = .some(value)
                print(value)
            case .failure:
                self.model = .none
            }
        }
    }
}
