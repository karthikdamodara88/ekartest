//
//  MapCoordinator.swift
//  EkarTest
//
//  Created by karthik damodara on 3/8/20.
//  Copyright © 2020 karthik damodara. All rights reserved.
//

import UIKit

class MapCoordinator: Coordinator {
    
    override func createRootViewController() -> UIViewController {
        let apiClient = APIClient(baseURL: BuildConfiguration.sharedInstance.baseURL)
        let interactor = MapInteractor(mapRepository: MapRespository(apiClient: apiClient))
        let viewModel = MapViewModel(with: interactor, coordinator: self)
        let viewController = MapViewController(with: viewModel)
        
        interactor.bind(with: viewController) { interactor, viewController in
            let viewModel = MapViewModel(with: interactor, coordinator: self)
            viewController.configure(with: viewModel)
        }
        
        return viewController
    }

}
